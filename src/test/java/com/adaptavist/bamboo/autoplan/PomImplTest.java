package com.adaptavist.bamboo.autoplan;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PomImplTest {
    @Test
    public void testPomGetProjectNameReturnsCorrectValue() {
        Pom pom = createNewSimplePom();
        assertEquals("Simple Plugin", pom.getProjectName());
    }

    @Test
    public void testPomGetDescriptionReturnsCorrectValue() {
        Pom pom = createNewSimplePom();
        assertEquals("This is the Simple Plugin for Confluence.", pom.getDescription());
    }

    @Test
    public void testPomGetScmLocationReturnsCorrectValue() {
        Pom pom = createNewSimplePom();
        assertEquals("scm:svn:https://adaptavist.jira.com/svn/SIMPLE/trunk", pom.getScmLocation());
    }

    @Test
    public void testThatTestGroupsAreNotNull(){
        Pom pom = createNewSimplePom();
        TestGroups testGroups = pom.getTestGroups();
        assertNotNull(testGroups);
    }

    @Test
    public void testThatTestGroupsAreReturnedCorrectly(){
        Pom pom = createNewSimplePom();
        TestGroups testGroups = pom.getTestGroups();
        Iterator<TestGroup> tgi = testGroups.iterator();
        assertTrue(tgi.hasNext());
    }

    @Test
    public void testThatAValidTestGroupExists(){
        Pom pom = createNewSimplePom();
        TestGroups testGroups = pom.getTestGroups();
        Iterator<TestGroup> tgi = testGroups.iterator();
        assertTrue(tgi.hasNext());
        TestGroup tg = tgi.next();
        assertNotNull(tg.getIncludes());
        assertNotNull(tg.getProductIds());
    }


    private Pom createNewSimplePom(){
        String pomFilename = "src/test/resources/simple-pom.xml";
        return new PomImpl(pomFilename);
    }
}
