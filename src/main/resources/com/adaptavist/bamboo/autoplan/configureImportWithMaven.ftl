[#-- @ftlvariable name="action" type="com.adaptavist.bamboo.autoplan.CheckoutAction" --]
[#-- @ftlvariable name="" type="com.adaptavist.bamboo.autoplan.CheckoutAction" --]

<html>
<head>
    [@ui.header pageKey='autoplan.create.maven.title' title=true /]
    <meta name="decorator" content="atl.general"/>
</head>
<body>
    [@ui.header pageKey='autoplan.create.maven.title' descriptionKey='autoplan.create.maven.description'/]

    [#if maven2BuilderAvailable]
        [@ww.form action='autoplanCheckout' namespace='/build/admin/create'
                  method="post" enctype="multipart/form-data"
                  titleKey='importWithMaven.pom.title'
                  submitLabelKey='global.buttons.import'
                  cancelUri='addPlan.action'
        ]
            [#assign repositoryList = filteredRepositories /]

            [@ww.select labelKey='repository.type' name='selectedRepository' toggle='true'
                        list = repositoryList listKey='key' listValue='name']
            [/@ww.select]
            [@ui.clear/]

            [#list repositoryList as repository]
                [@ui.bambooSection dependsOn='selectedRepository' showOn='${repository.key}']
                    ${repository.mavenPomAccessor.getMavenPomCheckoutAccessEditHtml(buildConfiguration)!}
                [/@ui.bambooSection]
            [/#list]

        [/@ww.form]
    [#else]
        [@ui.messageBox type="error"]
            [#if fn.hasAdminPermission()]
               [@ww.text name='importWithMaven.error.missingMaven2Builder']
                   [@ww.param][@ww.url action='configureSharedLocalCapabilities' namespace='/admin/agent' /][/@ww.param]
               [/@ww.text]
            [#else]
                [@ww.text name='importWithMaven.error.missingMaven2Builder.nonAdmin'/]
            [/#if]
        [/@ui.messageBox]
    [/#if]

</body>
</html>
