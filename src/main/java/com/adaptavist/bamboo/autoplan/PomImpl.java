package com.adaptavist.bamboo.autoplan;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class PomImpl implements Pom {
    final static Logger logger = LoggerFactory.getLogger(PomImpl.class);
    private Document doc;

    private final Namespace def = Namespace.getNamespace("def", "http://maven.apache.org/POM/4.0.0");
    private final Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

    public PomImpl(String filename) {
        SAXBuilder builder = new SAXBuilder();
        try {
            doc = builder.build(filename);
        } catch (JDOMException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public String getScmLocation() {
        return getTextValueOfElement("scm/def:developerConnection");
    }

    @Override
    public String getProjectName() {
        return getTextValueOfElement("name");
    }

    @Override
    public String getDescription() {
        return getTextValueOfElement("description");
    }

    @Override
    public TestGroups getTestGroups() {
        return createTestGroups();
    }

    private TestGroups createTestGroups() {
        TestGroupsImpl testGroups = new TestGroupsImpl();

         String textValue;
        try {
            XPath x = XPath.newInstance("//def:testGroups/def:testGroup");
            x.addNamespace(def);
            x.addNamespace(xsi);
            List<Element> list = (List<Element>) x.selectNodes(doc);
            for (Element element : list) {
                Element id = element.getChild("id",def);
                TestGroupImpl testGroup = new TestGroupImpl(id.getText());

                Element includes = element.getChild("includes", def);
                if (includes != null) {
                    List<Element> includesList = includes.getChildren("include",def);
                    for(Element include : includesList) {
                        testGroup.addInclude(include.getText());
                    }
                }

                Element productIds = element.getChild("productIds",def);
                if (productIds != null) {
                    List<Element> productIdsList = productIds.getChildren("productId",def);
                    for(Element productId : productIdsList) {
                        testGroup.addProductId(productId.getText());
                    }
                }

                testGroups.addTestGroup(testGroup);
            }
        } catch (JDOMException e) {
            logger.error(e.getMessage());
            textValue = "";
        }



        return testGroups;
    }


    private String getTextValueOfElement(String elementName) {
        assert elementName != null;

        String textValue;
        try {
            XPath x = XPath.newInstance("/def:project/def:"+elementName);
            x.addNamespace(def);
            x.addNamespace(xsi);
            List list = x.selectNodes(doc);
            if (list.isEmpty()) {
                textValue = "";
            } else {
                textValue = ((Element) list.get(0)).getText();
            }
        } catch (JDOMException e) {
            logger.error(e.getMessage());
            textValue = "";
        }
        return textValue;
    }
}
