package com.adaptavist.bamboo.autoplan;

public interface TestGroup {
    String getId();
    ProductIds getProductIds();

    Includes getIncludes();
}
