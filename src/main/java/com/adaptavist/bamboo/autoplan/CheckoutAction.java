package com.adaptavist.bamboo.autoplan;

import com.atlassian.bamboo.repository.MavenPomAccessorCapableRepository;
import com.atlassian.bamboo.util.BambooFileUtils;
import com.atlassian.bamboo.ww2.actions.admin.ImportMavenPlanCheckoutPomAction;
import com.atlassian.spring.container.ContainerManager;
import com.opensymphony.webwork.ServletActionContext;

import javax.servlet.http.HttpSession;
import java.io.File;

public class CheckoutAction extends ImportMavenPlanCheckoutPomAction {
    // for some reason unknown to me the bamboo action doesn't get fully injected unless explicitly autowire it from the ContainerManager
    public CheckoutAction() {
        ContainerManager.autowireComponent(this);
    }

    @Override
    public String doExecute() throws Exception {
        File tempDirectory = null;
        MavenPomAccessorCapableRepository pomAccessorRepository = (MavenPomAccessorCapableRepository) repositoryManager.getNewRepositoryInstance(getSelectedRepositoryKey());
        if (pomAccessorRepository == null)
        {
            throw new NullPointerException("Null reference to pomAccessorRepository - shall not happen");
        }

        pomAccessorRepository.populateFromConfig(getBuildConfiguration());

        tempDirectory = BambooFileUtils.createTempDirectory(this);
        File pomFile = pomAccessorRepository.getMavenPomAccessor().checkoutMavenPom(tempDirectory);
        HttpSession session = ServletActionContext.getRequest().getSession(true);
        session.setAttribute("pomfilename", pomFile.getAbsolutePath());
        session.setAttribute("pomtmpdirectory", tempDirectory.getAbsolutePath());
        return super.doExecute();
    }
}
